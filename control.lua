require 'util' -- Factorio-provided

require 'event'

require "builtin-silo"
require "builtin-playerspawn"

require 'hex-world'

local version = 1

Event.register(Event.core_events.init, function()
	global.version = version
end)

Event.register(Event.core_events.configuration_changed, function(event)
	if global.version ~= version then
		global.version = version
	end
end)
